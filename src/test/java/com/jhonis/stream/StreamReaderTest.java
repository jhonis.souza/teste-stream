package com.jhonis.stream;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

public class StreamReaderTest {

    @Test
    public void testeDeExemplo() {
        StreamReader streamReader = new StreamReader("aAbBABacafe");
        if (streamReader.hasNext()) {
            char next = streamReader.getNext();
            assertEquals('e', next);
            return;
        }
        fail("Caractere não encontrado");
    }

    @Test
    public void testComDoisCaracteresSeguindoARegra() {
        StreamReader streamReader = new StreamReader("aAbBABaceWo");
        char[] resultados = new char[2];
        int i = 0;
        while (streamReader.hasNext()) {
            resultados[i++] = streamReader.getNext();
        }

        assertEquals('e', resultados[0]);
        assertEquals('o', resultados[1]);
    }

    @Test
    public void testSemNenhumCaractereSeguindoARegra() {
        StreamReader streamReader = new StreamReader("aAbBABacafd");
        assertEquals(false, streamReader.hasNext());
    }

    @Test(expected = TypeNotPresentException.class)
    public void testSemNenhumCaractereSeguindoARegraEAcessandoDiretoOMetodoGetNext() {
        StreamReader streamReader = new StreamReader("aAbBABacDDIImmPSSsJJhhoonniSS");
        streamReader.getNext();
        fail("Algum caractere indevido foi encontrado");
    }
}
