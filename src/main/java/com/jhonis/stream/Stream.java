package com.jhonis.stream;

public interface Stream {
    char getNext();

    boolean hasNext();
}