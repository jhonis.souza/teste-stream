package com.jhonis.stream;

import java.util.*;

public class StreamReader implements Stream {

    private char[] entrada;
    private int indice;
    private ArrayList<Character> vogais;
    private List<Character> caracteresUnicos;
    private Character next;

    StreamReader(String entrada) {
        vogais = new ArrayList<>();
        vogais.addAll(Arrays.asList('a', 'A', 'e', 'E', 'i', 'I', 'o', 'O', 'u', 'U'));
        this.entrada = entrada.toCharArray();
        this.indice = 0;
        carregarCaracteresUnicos();
    }

    private void carregarCaracteresUnicos() {
        Map<Character, Integer> mapaCaracteres = new HashMap<>();
        for (char caractere : entrada) {
            mapaCaracteres.put(caractere, mapaCaracteres.getOrDefault(caractere, 0) + 1);
        }
        caracteresUnicos = new ArrayList<>();
        for (Character character : mapaCaracteres.keySet()) {
            if (mapaCaracteres.get(character) == 1) {
                caracteresUnicos.add(character);
            }
        }
    }

    @Override
    public char getNext() {
        if (next == null && !hasNext()) {
            throw new TypeNotPresentException("VogalConsoanteVogal", new Throwable("Não foi encontrada a formação de caracteres solicitada"));
        }
        return next;
    }

    @Override
    public boolean hasNext() {
        for (int i = indice; i < entrada.length - 1; i++) {
            char caractereAvaliado = entrada[i + 1];
            if (ehCaractereUnico(caractereAvaliado) && ehVogalConsoanteVogal(i)) {
                next = caractereAvaliado;
                indice = i + 1;
                return true;
            }
        }
        indice = entrada.length -1;
        return false;
    }

    private boolean ehCaractereUnico(char caractereAvaliado) {
        return caracteresUnicos.contains(caractereAvaliado);
    }

    private boolean ehVogalConsoanteVogal(int indice) {
        if (indice <= 0 || indice >= entrada.length) {
            return false;
        }
        char caractereAnterior = entrada[indice - 1];
        char caractereAtual = entrada[indice];
        char caractereProximo = entrada[indice + 1];
        return ehVogal(caractereAnterior) && ehConsoante(caractereAtual) && ehVogal(caractereProximo);
    }

    private boolean ehVogal(Character character) {
        return vogais.contains(character);
    }

    private boolean ehConsoante(Character character) {
        return !vogais.contains(character);
    }
}
